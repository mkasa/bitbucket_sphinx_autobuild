#!/usr/bin/python

################### CAUTION #####################
# THIS FILE SHOULD NOT BE ACCESSIBLE FROM HTTPD #
# PLEASE SEE THE DOCUMENT BELOW TO PROHIBIT     #
# DIRECT ACCESS TO THIS FILE (FROM WEB).        #
#################################################

# When you use apache, an example of .htaccess
# might be like this (without the hash at the beginning of the lines, of course):
#
#<FilesMatch "(\.(py|pyc)|~)$">
#	Order allow,deny
#	Deny from all
#</FilesMatch>
#AddHandler cgi-script .cgi
## This host is the BitBucket canon.
#allow from 63.246.22.222

################## Settings #####################
# Target absolute URL. This is usually "/(user name)/(repository name)". In case you do not want to check it, set it to None.
target_absolute_url = "/yourid/yourrep/"

# target_branches contain a configuration for each Target branch.
# The keys of target_branches are the names of the target branches for which we should rebuild the documents. i.e. you probably do not want to rebuild the public documents by 'git push' to debugging branches.
# A value for each key (target branch) is a dictionary in which the following information is described.
#   "build_dir"    : Build directory. This directory must exist and must be writable
#                    by httpd user (or use SuEXEC or equivalent)."
#   "deploy_dir"   : Deploy directory. The generated HTMLs are copied to this directory.
#   "tar_ball_dir" : Tar ball direcotory. The generated tar ball will be copied into deploy_dir+tar_ball_dir
#                    Specify None if you do not need tar balls.
#   "sphinx_document_dir" : Directory name in which Sphinx docs are located (in relative to repository root)
#   "make_dist"    : Command to create a disribution package.
#                    This is ignored when tar_ball_dir is None.
target_branches = {
"master": { "build_dir"    : "/home/apache/autobuild",
            "deploy_dir"   : "/var/www/your.domain.example.com/yourrep",
            "tar_ball_dir" : "dist",
            "sphinx_document_dir" : "doc",
            "make_dist"    : "make dist"
          }
}

# lock directory, in which a lock file is created
lock_dir = "/home/apache"

# Clone URL. This URL is used when the repository is cloned/pulled.
clone_url = "git@bitbucket.org:yourid/yourrep.git"

# Specify a user script that takes a single argument (host) and does ssh to that host (likely with the user's SSH identity).
# The user script will probably look like the following:
#    #!/bin/sh
#    ssh -i path_to_my_identity -o StrictHostKeyChecking=no $*
# Please bear in mind that "-o StringHostKeyCheking=no" is necessary if the host key of BitBucket is not stored yet for the httpd user, although you should actually obtain the host key of BitBucket (as the user of httpd) first and thus avoid the potential security breach.
# The reason why we need this is that git does not take arguments for ssh, though we often want to give an option to use a specific deployment key for ssh to BitBucket.
my_ssh_path = "/home/apache/autobuild/myssh"

# Whether output (debugging) log.
enable_debug_log = True

# Log file name. This file should not be accessed from web.
# Even if you disable enable_debug_log, access log is output to this file.
log_file_name = "out.txt"

################################################

if __name__ == "__main__":
    print "content-type: text/html\n"
    print "<p>I am a setting file, not designed for a stand-alone executable code.</p>"

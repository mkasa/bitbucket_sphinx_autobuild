#!/usr/bin/python

import os,sys,fcntl,sys
import cgi,cgitb,json,time,string,subprocess
from update_sphinx_settings import *

def rebuild(logf, branch_name, noredirect=False):
    def putm(message, section=False):
        if noredirect:
            if section:
                logf.write("<div style=\"color: green;\">")
            else:
                logf.write("<div style=\"color: navy;\">")
            logf.write(message)
            logf.write("</div>")
        else:
            logf.write(message)
        logf.flush()
    if not (branch_name in target_branches):
        putm("branch '%s' not found" % branch_name)
        return
    config = target_branches[branch_name]
    if not ("build_dir" in config):
        putm("\"build_dir\" is not defined in branch '%s'" % branch_name)
        return
    build_dir = config["build_dir"]
    if not ("deploy_dir" in config):
        putm("\"deploy_dir\" is not defined in branch '%s'" % branch_name)
        return
    deploy_dir = config["deploy_dir"]
    if not ("tar_ball_dir" in config):
        putm("\"tar_ball_dir\" is not defined in branch '%s'" % branch_name)
        return
    tar_ball_dir = config["tar_ball_dir"]
    if not ("make_dist" in config):
        if tar_ball_dir != None:
            putm("\"make_dist\" is not defined in branch '%s'" % branch_name)
            return
        make_dist_command = None
    else:
        make_dist_command = config["make_dist"]
    if not ("sphinx_document_dir" in config):
        putm("\"sphinx_document_dir\" is not defined in branch '%s'" % branch_name)
        return
    sphinx_document_dir = config["sphinx_document_dir"]

    putm("Start rebuilding at %s" % time.ctime(), True)
    try:
        putm("$ cd " + build_dir + "\n")
        os.chdir(build_dir)
    except:
        putm("Could not chdir to '%s' at %s\n" % (build_dir, time.ctime()))
        return
    local_repository_name = "rep"
    if os.path.exists(local_repository_name):
        putm("local repository found. Will 'git pull'.\n")
        try:
            putm("$ cd " + local_repository_name + "\n")
            os.chdir(local_repository_name)
        except:
            putm("Error on chdir to '%s'\n" % local_repository_name)
            return
        try:
            if noredirect:
                command_line = "git pull"
            else:
                command_line = "git pull 2>&1 > git_pull.log"
            putm("$ " + command_line + "\n")
            if my_ssh_path != None: command_line = ("GIT_SSH=" + my_ssh_path + " ") + command_line
            subprocess.check_call(command_line, shell=True, stdout=logf, stderr=logf)
            putm("$ cd ..\n")
            os.chdir("..")
        except:
            putm("Error occurred during 'git pull'\n")
            return
    else:
        putm("local repository does not exist. Will 'git clone'.", True)
        try:
            if noredirect:
                command_line = "git clone %s %s" % (clone_url, local_repository_name)
            else:
                command_line = "git clone %s %s 2>&1 > git.log" % (clone_url, local_repository_name)
            if my_ssh_path != None: command_line = ("GIT_SSH=" + my_ssh_path + " ") + command_line
            putm("$ " + command_line + "\n")
            subprocess.check_call(command_line, shell=True, stdout=logf, stderr=logf)
        except:
            putm("Error occurred during '%s'\n" % command_line)
            return
    try:
        putm("$ cd " + local_repository_name)
        os.chdir(local_repository_name)
        putm("$ cd " + local_repository_name + "\n")
    except:
        putm("Could not chdir to '%s'\n" % local_repository_name)
        return
    if tar_ball_dir != None:
        putm("Creating distribution package", True)
        try:
            subprocess.check_call(make_dist_command, shell=True, stdout=logf, stderr=logf)
        except:
            putm("Could not make dist ('%s')\n" % make_dist_command)
            return
        putm("Copying distribution packages\n")
        distribution_dir = os.path.join(deploy_dir, tar_ball_dir)
        if not os.path.exists(distribution_dir):
            try:
                os.mkdir(distribution_dir)
            except:
                putm("Could not mkdir %s" % distribution_dir)
                return
        try:
            if noredirect:
                command_line = "cp -f *.tar.?z* %s/" % distribution_dir
            else:
                command_line = "cp -f *.tar.?z* %s/ 2>&1 > dist_copy.log" % distribution_dir
            putm("$ " + command_line + "\n")
            subprocess.check_call(command_line, shell=True, stdout=logf, stderr=logf)
        except:
            putm("Could not copy distribution packages. Command line was:\n  %s\n" % command_line)
            return
    putm("Building Sphinx Document", True)
    try:
        putm("$ cd " + sphinx_document_dir)
        os.chdir(sphinx_document_dir)
    except:
        putm("Could not chdir to '%s'\n" % sphinx_document_dir)
        return
    try:
        subprocess.check_call("make html", shell=True, stdout=logf, stderr=logf)
    except:
        putm("Could not make html\n")
        return
    putm("Copying...", True)
    try:
        if noredirect:
            command_line = "cp -fr _build/html/* %s" % deploy_dir
        else:
            command_line = "cp -fr _build/html/* %s 2>&1 > html_copy.log" % deploy_dir
        putm("$ " + command_line)
        subprocess.check_call(command_line, shell=True, stdout=logf, stderr=logf)
    except:
        putm("Could not copy the generated HTMLs. Command line was:\n  %s\n" % command_line)
        return
    putm("Success at %s.\n" % time.ctime(), True)


# Main Program
cgitb.enable()
cgifs = cgi.FieldStorage()
print "Content-type: text/html\n\n";

# Locking Start
pid_file = os.path.join(lock_dir, ".lock")
lock_f = open(pid_file, "w")
retry_count = 0
while retry_count < 100:
    try:
        fcntl.lockf(lock_f, fcntl.LOCK_EX | fcntl.LOCK_NB)
        break
    except IOError:
        retry_count += 1
        time.sleep(5)
else:
    sys.exit(0)    

logf = open(log_file_name, "a+")

logf.write("access %s\n" % time.ctime()) # You can comment out this line if you do not need access log.

# payload is a data sent out from BitBucket (in JSON format)
payload = cgifs.getfirst('payload', 'NONE')

if enable_debug_log: logf.write(payload)
if enable_debug_log: logf.write("\nend payload\n")

# If you want to trigger the rebuild process only when the specified branch is affected, make the line below "if True".
if cgifs.getfirst('rebuild') != None:
    logf.write("Force rebuild.\n")
    print "<p>Forcing rebuild...</p>\n<pre>"
    sys.stdout.flush()
    branch_name = cgifs.getfirst('rebuild')
    rebuild(sys.stdout, branch_name, True)
else:
    try:
        json_payload = json.loads(payload)
        try:
            commit = json_payload['commits'][0]
            def rebuild_if_branch_name_match(branch_name):
                if branch_name in target_branches:
                    if enable_debug_log: logf.write("Start rebuild (branch: %s) %s\n" % (branch_name, time.ctime()))
                    rebuild(logf, branch_name)
            if "branch" in commit:
                branch_name = commit["branch"]
                rebuild_if_branch_name_match(branch_name)
            elif "branches" in commit:
                for branch_name in commit["branches"]:
                    rebuild_if_branch_name_match(branch_name)
            else:
                if enable_debug_log: logf.write("Do nothing because the branch(es) does not matter for us\n")
        except:
            logf.write("Some Error %s\n" % time.ctime())    
    except Exception, e:
        logf.write("JSON exception %s\n" % time.ctime())    
        print >>logf, e
        logf.write(payload + "\n")

logf.close()
print "</pre><p>Rebuild completed.</p>\n"
